<?php

namespace DsyRekognition;

/**
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
interface FacialAnalysisInterface {
    public function setConfidence(int $confidence): FacialAnalysisInterface;
    public function setAttributes(array $attributes): FacialAnalysisInterface;
    public function analyzeImagenAtPath(string $pathImage): FacialAnalysisInterface;
    public function facesCount(): int;
    public function facesHaveSunglasses(): array;
}
