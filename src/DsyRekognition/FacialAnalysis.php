<?php

namespace DsyRekognition;

use DsyRekognition\FacialAnalysisFactory;
use DsyRekognition\FacialAnalysisInterface;
use Aws\Rekognition\RekognitionClient;
use Aws\Rekognition\Exception\RekognitionException;
use Aws\ResultInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Description of FacialAnalysis
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class FacialAnalysis implements FacialAnalysisInterface
{
    const FACE_DETECT_ATTRIBUTE_ALL = 'ALL';
    const FACE_DETECT_ATTRIBUTE_DEFAULT = 'DEFAULT';
    const FACE_DETECT_ATTRIBUTE_BOUNDING_BOX = 'BOUNDINGBOX';
    const FACE_DETECT_ATTRIBUTE_CONFIDENCE = 'CONFIDENCE';
    const FACE_DETECT_ATTRIBUTE_POSE = 'POSE';
    const FACE_DETECT_ATTRIBUTE_QUALITY = 'QUALITY';
    const FACE_DETECT_ATTRIBUTE_LANDMARKS = 'LANDMARKS';

    /**
     * 5 MBytes
     */
    const FACES_DETECT_IMAGE_LIMIT_SIZE = '5000000';
    const FACES_DETECT_IMAGE_FORMAT_JPG = 'jpg';
    const FACES_DETECT_IMAGE_FORMAT_JPEG = 'jpeg';
    const FACES_DETECT_IMAGE_FORMAT_PNG = 'png';

    /**
     * @var RekognitionClient
     */
    private $client;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var integer
     */
    private $confidence;

    /**
     * @var ResultInterface
     */
    private $detectFaceReponse;

    /**
     * @return string[]
     */
    public static function getFaceDetectedAttributes()
    {
        return [
            self::FACE_DETECT_ATTRIBUTE_DEFAULT,
            self::FACE_DETECT_ATTRIBUTE_BOUNDING_BOX,
            self::FACE_DETECT_ATTRIBUTE_CONFIDENCE,
            self::FACE_DETECT_ATTRIBUTE_LANDMARKS,
            self::FACE_DETECT_ATTRIBUTE_POSE,
            self::FACE_DETECT_ATTRIBUTE_QUALITY,
            self::FACE_DETECT_ATTRIBUTE_ALL,
        ];
    }

    /**
     * @return atring[]
     */
    public static function getImageFormats()
    {
        return [
            self::FACES_DETECT_IMAGE_FORMAT_JPEG,
            self::FACES_DETECT_IMAGE_FORMAT_JPG,
            self::FACES_DETECT_IMAGE_FORMAT_PNG
        ];
    }

    /**
     * Constructor
     * @param FacialAnalysisFactory $factory
     */
    public function __construct(FacialAnalysisFactory $factory)
    {
        $this->client = $factory->getFacialAnalysisClient();
        $this->confidence = 80;
        $this->attributes = [self::FACE_DETECT_ATTRIBUTE_ALL];
    }

    /**
     * Set Attributes
     * @param string[] $attributes
     * @return FacialAnalysisInterface
     * @throws Exception
     */
    public function setAttributes(array $attributes): FacialAnalysisInterface
    {
        if (count($attributes) > 0 && !$this->isValidAttributes($attributes)) {
            throw new \Exception("Invalid attributes");
        }
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * Set Confidence
     * @param int $confidence
     * @return FacialAnalysisInterface
     * @throws Exception
     */
    public function setConfidence(int $confidence): FacialAnalysisInterface
    {
        if ($confidence < 1 && $confidence > 100) {
            throw new \Exception("Confidence out range (1 .. 100) porcent");
        }
        $this->confidence = $confidence;
        return $this;
    }

    /**
     * Analisis de la imagen
     * @param string $pathImage
     * @return FacialAnalysisInterface
     * @throws Exception
     */
    public function analyzeImagenAtPath(string $pathImage): FacialAnalysisInterface
    {
        $file = file_get_contents($pathImage);
        try {
            $this->detectFaceReponse = $this->client->detectFaces([
                'Attributes' => $this->attributes,
                'Image' => [// REQUIRED
                    'Bytes' => $file,
//                    'S3Object' => [
//                        'Bucket' => '<string>',
//                        'Name' => '<string>',
//                        'Version' => '<string>',
//                    ],
                ],
            ]);
        } catch (RekognitionException $e) {
//            throw new ;
//            dump($e);
//            die;
        } catch (AccessDeniedException $e) {
//            dump($e);
//            die;
        };
        return $this;
    }

    /**
     * Pregunta al Json detectFaceReponse la cantidad de caras que se encontraron
     * en la imagen.
     * @return int
     */
    public function facesCount(): int
    {
        return $this->detectFaceReponse->search('length(FaceDetails)');
    }

    /**
     * Pregunta a detectFaceReponse si las caras tienes lentes de sol tomendao en
     * cuenta el porcentaje de confidence establecido
     * Devuelve un arreglo de boleanos
     * @return boolean[]
     */
    public function facesHaveSunglasses(): array
    {
        return $this->detectFaceReponse->search("FaceDetails[?Sunglasses.Confidence>=`{$this->confidence}`].[Sunglasses.Value] | @[]");
    }

    /**
     * Validacion de Atributos
     * @param array $attributes
     * @return boolean
     */
    private function isValidAttributes(array $attributes)
    {
        foreach ($attributes as $attr) {
            if (!in_array(strtoupper($attr), $this->getFaceDetectedAttributes())) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Get Reponse Face Detect
     * @return ResultInterface
     */
    public function getFaceDetectResponse()
    {
        return $this->detectFaceReponse;
    }
}
