<?php

namespace DsyRekognition;

use Aws\Rekognition\RekognitionClient;
use Aws\AwsClient;
use Aws\Credentials\CredentialsInterface;
use Aws\Credentials\Credentials;

/**
 * Description of FacialAnalysisFactory
 *
 * @author snake77se <yosip.curiel@dsarhoya.cl>
 */
class FacialAnalysisFactory {

    /**
     * @var RekognitionClient 
     */
    private $awsRekognitionClient;

    /**
     * @var string
     */
    private $region;

    /**
     *
     * @var CredentialsInterface
     */
    private $credentials;

    /**
     * @param string $region
     */
    public function __construct($awsAccessKey, $awsSecretKey, $region) {
        $this->region = $region;
        $this->credentials = new Credentials($awsAccessKey, $awsSecretKey);
    }

    /**
     * @return CredentialsInterface
     */
    public function getCredentails() {
        return $this->credentials;
    }

    /**
     * Get FacialAnalysisClient (RekognitionClient)
     * @return RekognitionClient
     */
    function getFacialAnalysisClient() {
        $this->awsRekognitionClient = new RekognitionClient(array(
            'region' => $this->region,
            'version' => 'latest',
            'credentials' => $this->credentials,
        ));
        return $this->awsRekognitionClient;
    }
    
    

}
