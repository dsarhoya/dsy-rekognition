<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

/**
 * Description of UploadFileType
 *
 * @author snake77se
 */
class UploadFileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'label'=>false,
                'required'=>true,
                'constraints'=>[
                    new File([
                        'maxSize'=>'5M',
                        'mimeTypes'=> ['image/png', 'image/jpeg'],
                        'mimeTypesMessage'=>"Tipo de Archivo no permitido",
                    ]),
                ]
            ])
           ->add('submit', SubmitType::class, array(
               'label'=>'Analizar',
               'attr'=>array('class'=>'btn btn-success'),
           ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
        ));
    }

    public function getBlockPrefix()
    {
        return 'dsy_rek_upload_file';
    }
}
