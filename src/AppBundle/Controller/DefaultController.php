<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\UploadFileType;
use DsyRekognition\FacialAnalysis;
use DsyRekognition\FacialAnalysisFactory;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // // replace this example code with whatever you need
        // return $this->render('default/index.html.twig', [
        //     'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        // ]);
        return $this->redirectToRoute('facial_analysis_index');
    }
    
    /**
     * Rekognition example
     * @Route("/facial-analysis", name="facial_analysis_index")
     * @Method({"GET","POST"})
     * @param  Request $request
     * @return Response
     */
    public function facialAnalysisAction(Request $request)
    {
        $facesSunglasses = $facesCount = $facialAnalysisResult = null;
        $form = $this->createForm(UploadFileType::class);
        $confidence = 80; //Confidence permitido
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            $factory = $this->container->get('dsy.rekognition.facial_analysis.factory');
            $facialAnalysis = new FacialAnalysis($factory);
            $facialAnalysis
                ->setAttributes([FacialAnalysis::FACE_DETECT_ATTRIBUTE_ALL])
                ->setConfidence($confidence)
                ->analyzeImagenAtPath($file->getPathname());
            $facesCount = $facialAnalysis->facesCount();
            $facesSunglasses = $facialAnalysis->facesHaveSunglasses();
            $facialAnalysisResult = $facialAnalysis->getFaceDetectResponse();
        }
        return $this->render('default/facialAnalysis.html.twig', [
            'form' => $form->createView(),
            'facesSunglasses' => $facesSunglasses,
            'facesCount' => $facesCount,
            'facialAnalysisResult' => $facialAnalysisResult,
        ]);
    }
}
